import { StyleSheet } from 'react-native';
import { color, fontSize, padding, responsivePixel } from '@helpers/index';

export default StyleSheet.create({
  container: {
    flex: 1,
    padding: padding.p32,
    justifyContent: 'space-around',
    backgroundColor: color.lightGrayBorder,
  },
  logoIcon: { position: 'absolute', left: padding.p32, top: responsivePixel(72) },
  textTitle: { fontWeight: 'bold', fontSize: 44 },
  buttonGoogleStyle: { padding: padding.p16, backgroundColor: color.white, borderRadius: 8 },
  buttonAppleStyle: {
    padding: padding.p16,
    backgroundColor: color.black,
    borderRadius: 8,
  },
  textButton: { fontWeight: '600', fontSize: fontSize.f16, marginHorizontal: padding.p8 },
  borderButton: {
    borderColor: color.grayBorder,
    marginBottom: padding.p16,
    borderWidth: 1.5,
  },
});
