import React, { useEffect, useState } from 'react';
import { View } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import { color, fontSize } from '@helpers/index';
import { AppButton, AppImage, AppText } from '@components/index';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '@redux/reducers';
import Spinner from 'react-native-loading-spinner-overlay';
import actionTypes from '@redux/actionTypes';
import styles from './styles';
import { loginSuccess } from '@redux/actions/userActions';

interface Props {}

const SignInScreen = React.memo((props: Props) => {
  const { t } = useTranslation();
  const userReducer = useSelector((state: RootState) => state.userReducer);
  const dispatch = useDispatch();
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    if (userReducer.type === actionTypes.LOGIN_SUCCESS || userReducer.type === actionTypes.LOGIN_FAILED) {
      setLoading(false);
    }
  }, [userReducer]);

  const onSignIn = () => {
    setLoading(true);
    setTimeout(() => {
      dispatch(loginSuccess({ userName: 'halu' }, true, {}));
    }, 1000);
  };

  return (
    <SafeAreaView style={styles.container}>
      <Spinner visible={loading} animation="slide" color={color.primary} />
      <View style={styles.logoIcon}>
        <AppImage style={{ width: 100, height: 100 }} source={{ uri: undefined }} />
      </View>
      <View />
      <AppText fontSize={fontSize.f36}>{t('nav:login')}</AppText>
      <View>
        <AppButton
          onPress={onSignIn}
          buttonStyle={styles.buttonGoogleStyle}
          containerStyle={styles.borderButton}
          title={t('nav:login')}
          titleStyle={[{ color: color.black }, styles.textButton]}
        />
      </View>
    </SafeAreaView>
  );
});

export default SignInScreen;
