import React from 'react';
import { View } from 'react-native';
import { AppButton } from '@components/index';
import styles from './styles';
import { useDispatch } from 'react-redux';
import { logout } from '@redux/actions/userActions';
const Home = () => {
  const dispatch = useDispatch();

  return (
    <View style={styles.container}>
      <AppButton
        title={'Logout'}
        onPress={() => {
          dispatch(logout());
        }}
      />
    </View>
  );
};

export default Home;
