import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import RootStackScreen from '@navigation/index';
import { store, persistor } from '../store';
import { LogBox } from 'react-native';
import { PersistGate } from 'redux-persist/integration/react';
import { Provider } from 'react-redux';
import SplashScreen from 'react-native-splash-screen';
import { I18nextProvider } from 'react-i18next';
import { i18n } from './context/locales';

LogBox.ignoreLogs([
  'VirtualizedLists should never be nested',
  'Remote debugger is in a background tab which may cause apps to perform slowly',
  'DevTools failed to load SourceMap',
]);

export default () => {
  const [isLoading, setIsLoading] = React.useState(true);

  React.useEffect(() => {
    setTimeout(() => {
      setIsLoading(false);
      SplashScreen.hide();
    }, 800);
  }, []);

  if (isLoading) {
    return null;
  }

  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <I18nextProvider i18n={i18n}>
          <NavigationContainer>
            <RootStackScreen />
          </NavigationContainer>
        </I18nextProvider>
      </PersistGate>
    </Provider>
  );
};
