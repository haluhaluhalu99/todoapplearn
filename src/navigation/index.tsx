import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { RootState } from '@redux/reducers';
import { useSelector } from 'react-redux';
import _ from 'lodash';
import { AppRoutes } from './appRoutes';
import { SignInScreen, Home } from '@screen/index';

const Tabs = createBottomTabNavigator();

const AuthStack = createStackNavigator();
const HomeStack = createStackNavigator();
const SearchStack = createStackNavigator();
const ProfileStack = createStackNavigator();

const Drawer = createDrawerNavigator();

const RootStack = createStackNavigator();

const RootStackScreen = () => {
  const userReducer = useSelector((state: RootState) => state.userReducer);
  const isLogin = !_.isEmpty(userReducer.data);
  return (
    <RootStack.Navigator headerMode="none">
      {isLogin ? (
        <RootStack.Screen
          name={AppRoutes.APP}
          component={Home}
          options={{
            animationEnabled: true,
          }}
        />
      ) : (
        <RootStack.Screen
          name={AppRoutes.AUTH}
          component={SignInScreen}
          options={{
            animationEnabled: true,
          }}
        />
      )}
    </RootStack.Navigator>
  );
};
export default RootStackScreen;
