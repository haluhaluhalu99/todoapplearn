export enum AppRoutes {
  APP = 'App',
  AUTH = 'Auth',
  LOGIN = 'Login',
  REGISTER = 'Register',
}
