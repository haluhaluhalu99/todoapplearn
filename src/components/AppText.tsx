import React from 'react';
import { StyleSheet, Text, TextProps } from 'react-native';
import { fontSize } from '@helpers/index';

export type AppTextProps = {
  value?: string | number | null;
  fontSize?: number;
  color?: string;
  fontWeight?: FontWeightType;
  fontFamily?: FontType;
} & TextProps;
// if customize fonts
export type FontType =
  | 'SFProText-Semibold'
  | 'SFProText-Regular'
  | 'SFProText-Bold'
  | 'SFProText-Light'
  | 'SFProText-Medium';
export type FontWeightType = 'normal' | 'medium' | 'bold' | 'black' | 'light' | 'semibold';
export const defaultProps: Partial<AppTextProps> = {
  fontSize: fontSize.f14,
};

const AppText: React.FC<AppTextProps> = React.memo(
  ({ children, color, value, fontFamily, fontWeight, fontSize, ...props }) => {
    return (
      <Text
        {...props}
        allowFontScaling={false}
        style={StyleSheet.flatten([
          {
            fontSize,
            color,
            // fontFamily: 'SFProText-Regular',
            lineHeight: fontSize ? fontSize + 4 : fontSize,
          },
          // fontWeight === 'medium' && {
          //   fontFamily: 'SFProText-Medium',
          // },
          // fontWeight === 'bold' && {
          //   fontFamily: 'SFProText-Bold',
          // },
          // fontWeight === 'light' && {
          //   fontFamily: 'SFProText-Light',
          // },
          // fontWeight === 'semibold' && {
          //   fontFamily: 'SFProText-Semibold',
          // },
          props.style,
        ])}>
        {value ? value : children}
      </Text>
    );
  },
);

export default AppText;
